# React-starter

<h2>Tools</h2>

- Webpack
  For bundling in development

- Rollup
  For bundling production code

- React
  A JavaScript library for building user interfaces

- Workbox
  For creating service workers

<h4>Get Started</h4>

- Install Node Packages.

  - run npm install

- Start the app.

  - npm start or yarn start

- Production Build (built files are placed in the app dirrectory)

  - npm run build

<h4>:fire: Enjoy</h4>

Note:

APis domains can be changed depending on what environment thats running development or production you can connect to both the local and remote server at src > actions > utils. (just comment out the evironment you in)
